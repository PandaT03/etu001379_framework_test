<%-- 
    Document   : liste_Etudiant
    Created on : 18 nov. 2022, 22:38:23
    Author     : Tiavina P(anda)C
--%>

<%@page import="modele.Etudiant"%>
<%       
    Etudiant[] liste_etudiant =(Etudiant[])request.getAttribute("liste");
%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Test Framework</title>
    </head>
    <body>
        <h1>Liste Etudiant</h1>
        <ul>Voici les Etudiants :
        <% for(int i=0;i<liste_etudiant.length;i++) { %>
        <li>Nom : <% out.print(liste_etudiant[i].getNom_Etudiant()); %>, Age : <% out.print(liste_etudiant[i].getAge_Etudiant()); %></li>
        <% } %>
        </ul>
    </body>
</html>
