/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template fichier, choose Tools | Templates
 * and open the template in the editor.
 */
package java.framework;

import java.framework.Url_annotation;
import java.framework.UrlNotSupportedException;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Tiavina P(anda)C
 */
public class Utilitaire {
    
    public static String retrieveUrlFromRawUrl(String rawUrl) {
        String[] trueUrl = rawUrl.split("/");
        if (trueUrl.length != 0) {
            return trueUrl[trueUrl.length - 1];
        }
        return null;
    }
    
    //------------------------------------Browse all classes on ClassPath-----------------------------------------
    private static ArrayList<Class<?>> All_Classes(File dossier, String Nom_Package) {
        
        if (!dossier.exists()) 
        {
            return null;
        }
        
        ArrayList<Class<?>> classes = new ArrayList<>();
        File[] liste_fichier = dossier.listFiles();
        for (int i=0;i<liste_fichier.length;i++) 
        {
            if (liste_fichier[i].isDirectory()) 
            {
                assert !liste_fichier[i].getName().contains(".");
                classes.addAll(All_Classes(liste_fichier[i],(!Nom_Package.equals("") ? Nom_Package + "." : Nom_Package) + liste_fichier[i].getName()));
            } 
            else if (liste_fichier[i].getName().endsWith(".class")) 
            {
                try {
                    classes.add(Class.forName(Nom_Package + '.' + liste_fichier[i].getName().substring(0, liste_fichier[i].getName().length() - 6)));
                } catch (ClassNotFoundException e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return classes;
    }

    private static ArrayList<Class<?>> getClasses(String Nom_Package) {
        
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String chemin = Nom_Package.replace('.', '/');
        Enumeration<URL> ressources = null;
        try {
            ressources = classLoader.getResources(chemin);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        List<File> dirs = new ArrayList<>();
        while (ressources.hasMoreElements()) 
        {
            URL resource = ressources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class<?>> classes = new ArrayList<>();
        dirs.forEach((directory) -> 
        {
            classes.addAll(All_Classes(directory, Nom_Package));
        });
        return classes;
    }
    //---------------------------------------------------------------------------------------------------------
    //-----------------------------------------Fill hashMap attributes from context----------------------------
    public void fillListAssociation(ArrayList<HashMap<String, String>> Liste_Association_URL) {

        ArrayList<Class<?>> classes = getClasses("");

        for (int i=0;i<classes.size();i++) 
        {
            Method[] methods = classes.get(i).getMethods();
            for (int i2=0;i2<methods.length;i2++) 
            {
                Url_annotation urlAnnotation = methods[i2].getAnnotation(Url_annotation.class);
                if (urlAnnotation != null) 
                {
                    HashMap<String, String> association = new HashMap<>();
                    association.put("Url", urlAnnotation.value());
                    association.put("Class", classes.get(i).getName());
                    association.put("Method", methods[i2].getName());
                    Liste_Association_URL.add(association);
                }
            }
        }
    }
    //---------------------------------------------------------------------------------------------------------
    //---------------------------------------Check if 'url' is already on hashMap from context-----------------
    public static HashMap<String, String> check_Url(ArrayList<HashMap<String, String>> Liste_Association_URL, String true_Url) throws UrlNotSupportedException {
        
        for (int i = 0; i < Liste_Association_URL.size(); i++) 
        {
            if (Liste_Association_URL.get(i).get("Url").equals(true_Url)) 
                return Liste_Association_URL.get(i);
        }
        throw new UrlNotSupportedException("Oops!Url not supported");
    }
    //---------------------------------------------------------------------------------------------------------
}
