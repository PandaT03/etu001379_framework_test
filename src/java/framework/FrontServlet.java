/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java.framework;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.framework.ModelView;
import java.framework.Utilitaire;

/**
 *
 * @author Tiavina P(anda)C
 */
@WebServlet(name = "FrontServlets", urlPatterns = {"*.do"})
public class FrontServlet extends HttpServlet {

    @Override
    public void init() throws ServletException {
        ServletContext context = this.getServletContext();
        ArrayList<HashMap<String, String>> Liste_Association_URL = new ArrayList<>();
        new Utilitaire().fillListAssociation(Liste_Association_URL);
        context.setAttribute("Liste_Association_URL", Liste_Association_URL);
    }
    
    public String upperCaseFirst(String mot)
    {
        char[] arr=mot.toCharArray();
        arr[0]=Character.toUpperCase(arr[0]);
        return new String(arr);
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        ServletContext context = this.getServletContext();
        response.setContentType("text/plain");
        HashMap<String, String> association_lien = null;
        String lien_dispat = "";
        
        //---------------retrieveUrlFromRawUrl--------
        String Url = Utilitaire.retrieveUrlFromRawUrl(request.getRequestURI());
        
        //---------------recuperer la valeur du variable dans context
        ArrayList<HashMap<String, String>> Liste_Association_URL = (ArrayList<HashMap<String, String>>) context.getAttribute("Liste_Association_URL");
        
        try {
            
            //-----------check url--------------------
            association_lien = Utilitaire.check_Url(Liste_Association_URL, Url);
            
            //-----------new Instance de la classe----
            Class<?> loadClass = Class.forName(association_lien.get("Class"));
            Object o = loadClass.newInstance();
            
            //-----------Mettre les parametres dans les attributs de la classe
            Field[] fields = o.getClass().getDeclaredFields();
            Method[] methods = o.getClass().getMethods();
            for (int i = 0; i < fields.length; i++) {
                if (request.getParameter(fields[i].getName()) != null) {
                    String setValue = request.getParameter(fields[i].getName());
                    for (int i2 = 0; i2 < methods.length; i2++) {
                        if (methods[i2].getName().equals("set" + upperCaseFirst(fields[i].getName()))) {
                            if (fields[i].getType().getSimpleName().equals("String")) {
                                methods[i2].invoke(o, setValue);
                            } else if (fields[i].getType().getSimpleName().equals("Integer")) {
                                methods[i2].invoke(o, Integer.parseInt(setValue));
                            } else if (fields[i].getType().getSimpleName().equals("Double")) {
                                methods[i2].invoke(o, Double.parseDouble(setValue));
                            }
                            break;
                        }
                    }
                }
            }
            
            //-----------invoke du methode------------
            Method method = o.getClass().getDeclaredMethod(association_lien.get("Method"), null);
            ModelView mv = (ModelView) method.invoke(o);
            
            //-----------setUrl-----------------------
            lien_dispat = mv.getUrl();
            
            //-----------setAttribute data------------
            for (HashMap.Entry<String, Object[]> entry : mv.getData().entrySet()) 
            {
                String key = entry.getKey();
                Object[] value = entry.getValue();
                request.setAttribute(key, value);
            }
            
        } catch (Exception e) {
            request.setAttribute("Exception", e);
            lien_dispat = "exception.jsp";
        } finally {
            RequestDispatcher dispat = request.getRequestDispatcher(lien_dispat);
            dispat.forward(request, response);
        }
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
