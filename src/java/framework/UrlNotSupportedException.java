/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java.framework;

/**
 *
 * @author Tiavina P(anda)C
 */
public class UrlNotSupportedException extends Exception{
    
    public UrlNotSupportedException(String new_Message) {
        super(new_Message);
    }
}
