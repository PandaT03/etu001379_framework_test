/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package java.modele;

import java.framework.Url_annotation;
import java.framework.ModelView;
import java.util.HashMap;

/**
 *
 * @author Tiavina P(anda)C
 */
public class Etudiant {
    
    private int id_Etudiant;
    private String nom_Etudiant;
    private int age_Etudiant;
    private String date_naissance_Etudiant;

    public Etudiant() {
    }
    
    public Etudiant(int id_Etudiant, String nom_Etudiant, int age_Etudiant, String date_naissance_Etudiant) {
        this.id_Etudiant = id_Etudiant;
        this.nom_Etudiant = nom_Etudiant;
        this.age_Etudiant = age_Etudiant;
        this.date_naissance_Etudiant = date_naissance_Etudiant;
    }

    public String getDate_naissance_Etudiant() {
        return date_naissance_Etudiant;
    }

    public void setDate_naissance_Etudiant(String date_naissance_Etudiant) {
        this.date_naissance_Etudiant = date_naissance_Etudiant;
    }

    public int getId_Etudiant() {
        return id_Etudiant;
    }

    public void setId_Etudiant(int id_Etudiant) {
        this.id_Etudiant = id_Etudiant;
    }

    public String getNom_Etudiant() {
        return nom_Etudiant;
    }

    public void setNom_Etudiant(String nom_Etudiant) {
        this.nom_Etudiant = nom_Etudiant;
    }

    public int getAge_Etudiant() {
        return age_Etudiant;
    }

    public void setAge_Etudiant(int age_Etudiant) {
        this.age_Etudiant = age_Etudiant;
    }
    
    @Url_annotation(value="list_Etudiant.do")
    public ModelView list(){
       
       Etudiant[] etudiant=new Etudiant[3];
       etudiant[0].setId_Etudiant(1);
       etudiant[0].setAge_Etudiant(10);
       etudiant[0].setDate_naissance_Etudiant("2012-03-05");
       etudiant[0].setNom_Etudiant("Dimitri");
       
       etudiant[1].setId_Etudiant(2);
       etudiant[1].setAge_Etudiant(10);
       etudiant[1].setDate_naissance_Etudiant("2012-06-07");
       etudiant[1].setNom_Etudiant("Robert");
       
       etudiant[2].setId_Etudiant(3);
       etudiant[2].setAge_Etudiant(11);
       etudiant[2].setDate_naissance_Etudiant("2011-05-09");
       etudiant[2].setNom_Etudiant("Jean");
       
       HashMap<String,Object[]> hash = new HashMap<String,Object[]>();
       hash.put("liste",etudiant);
       ModelView mv = new ModelView();
       mv.setUrl("liste_Etudiant.jsp");
       mv.setData(hash);
   
       return mv;
    }
    
    @Url_annotation(value="ajout_Etudiant.do")
    public ModelView ajoutMode(){
        
       ModelView mv = new ModelView();
       HashMap<String,Object[]> hash = new HashMap<String,Object[]>();
       mv.setUrl("ajout_Etudiant.jsp");
       mv.setData(hash);
       
       return mv;
    }
}
